<?php

/**
 * @file
 * uw_vocab_feds_event_ages.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function uw_vocab_feds_event_ages_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_rdf_default_mappings().
 */
function uw_vocab_feds_event_ages_rdf_default_mappings() {
  $schemaorg = array();

  // Exported RDF mapping: event_ages.
  $schemaorg['taxonomy_term']['event_ages'] = array(
    'rdftype' => array(
      0 => 'skos:Concept',
    ),
    'name' => array(
      'predicates' => array(
        0 => 'rdfs:label',
        1 => 'skos:prefLabel',
      ),
    ),
    'description' => array(
      'predicates' => array(
        0 => 'skos:definition',
      ),
    ),
    'vid' => array(
      'predicates' => array(
        0 => 'skos:inScheme',
      ),
      'type' => 'rel',
    ),
    'parent' => array(
      'predicates' => array(
        0 => 'skos:broader',
      ),
      'type' => 'rel',
    ),
  );

  return $schemaorg;
}
