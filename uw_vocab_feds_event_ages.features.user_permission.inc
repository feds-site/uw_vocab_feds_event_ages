<?php

/**
 * @file
 * uw_vocab_feds_event_ages.features.user_permission.inc
 */

/**
 * Implements hook_user_default_permissions().
 */
function uw_vocab_feds_event_ages_user_default_permissions() {
  $permissions = array();

  // Exported permission: 'define view for terms in event_ages'.
  $permissions['define view for terms in event_ages'] = array(
    'name' => 'define view for terms in event_ages',
    'roles' => array(
      'administrator' => 'administrator',
      'site manager' => 'site manager',
    ),
    'module' => 'tvi',
  );

  // Exported permission: 'define view for vocabulary event_ages'.
  $permissions['define view for vocabulary event_ages'] = array(
    'name' => 'define view for vocabulary event_ages',
    'roles' => array(
      'administrator' => 'administrator',
      'site manager' => 'site manager',
    ),
    'module' => 'tvi',
  );

  // Exported permission: 'delete terms in event_ages'.
  $permissions['delete terms in event_ages'] = array(
    'name' => 'delete terms in event_ages',
    'roles' => array(
      'administrator' => 'administrator',
      'site manager' => 'site manager',
    ),
    'module' => 'taxonomy',
  );

  // Exported permission: 'edit terms in event_ages'.
  $permissions['edit terms in event_ages'] = array(
    'name' => 'edit terms in event_ages',
    'roles' => array(
      'administrator' => 'administrator',
      'site manager' => 'site manager',
    ),
    'module' => 'taxonomy',
  );

  return $permissions;
}
