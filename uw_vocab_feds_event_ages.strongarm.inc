<?php

/**
 * @file
 * uw_vocab_feds_event_ages.strongarm.inc
 */

/**
 * Implements hook_strongarm().
 */
function uw_vocab_feds_event_ages_strongarm() {
  $export = array();

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'metatag_enable_taxonomy_term__event_ages';
  $strongarm->value = TRUE;
  $export['metatag_enable_taxonomy_term__event_ages'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'uuid_features_entity_taxonomy_term_event_ages';
  $strongarm->value = 'event_ages';
  $export['uuid_features_entity_taxonomy_term_event_ages'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'uuid_features_file_taxonomy_term_event_ages';
  $strongarm->value = 0;
  $export['uuid_features_file_taxonomy_term_event_ages'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'uw_data_web_pages_setup';
  $strongarm->value = array(
    'home' => 1,
    'about' => 1,
  );
  $export['uw_data_web_pages_setup'] = $strongarm;

  return $export;
}
