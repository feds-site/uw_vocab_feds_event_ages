<?php

/**
 * @file
 * uw_vocab_feds_event_ages.features.uuid_term.inc
 */

/**
 * Implements hook_uuid_features_default_terms().
 */
function uw_vocab_feds_event_ages_uuid_features_default_terms() {
  $terms = array();

  $terms[] = array(
    'name' => 'All Ages',
    'description' => '',
    'format' => 'uw_tf_standard',
    'weight' => 0,
    'uuid' => '601fe54a-ed38-46d1-b5cd-208cbe34dfc6',
    'vocabulary_machine_name' => 'event_ages',
    'path' => array(
      'pathauto' => 1,
    ),
    'url_alias' => array(
      0 => array(
        'alias' => 'event-ages/all-ages',
        'language' => 'und',
      ),
    ),
  );
  $terms[] = array(
    'name' => '19+',
    'description' => '',
    'format' => 'uw_tf_standard',
    'weight' => 1,
    'uuid' => '84e656fd-4274-4890-82dd-7669b3e7c4b8',
    'vocabulary_machine_name' => 'event_ages',
    'path' => array(
      'pathauto' => 1,
    ),
    'url_alias' => array(
      0 => array(
        'alias' => 'event-ages/19',
        'language' => 'und',
      ),
    ),
  );
  return $terms;
}
